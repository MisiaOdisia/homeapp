package homeapp.shoppinglist.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import homeapp.shoppinglist.model.data.ItemRequest;
import homeapp.shoppinglist.model.data.response.ItemResponse;
import homeapp.shoppinglist.service.ShoppingListItemService;

@RestController
@RequestMapping("api/v1/shoppinglist")
public class ShoppingListItemController {
	
	@Autowired
	private ShoppingListItemService shoppingListItemService;
	
	@GetMapping("{shoppingListId}/item")
	public ResponseEntity<List<ItemResponse>> getItems(@PathVariable(value = "shoppingListId") String id) {
		return shoppingListItemService.getItems(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping("{shoppingListId}/item/{itemId}")
	public ResponseEntity<ItemResponse> getItemById(@PathVariable(value = "shoppingListId") String shoppingListid,
			   								@PathVariable(value = "itemId") String itemId) {
		return shoppingListItemService.getItemById(shoppingListid, itemId)
				.map(ResponseEntity::ok)				
				.orElse(ResponseEntity.notFound().build());
	}	
	
	@PostMapping("{shoppingListId}/item")
	public ResponseEntity<ItemResponse> addItemToList(@PathVariable(value = "shoppingListId") String shoppingListId, @Valid @RequestBody ItemRequest item) {
		return shoppingListItemService.addItemToList(shoppingListId, item)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("{shoppingListId}/item/{itemId}")
	public ResponseEntity<Void> deleteItemFromList(@PathVariable(value = "shoppingListId") String shoppingListId,
												   @PathVariable(value = "itemId") String itemId) {
		return shoppingListItemService.removeItemFromList(shoppingListId, itemId)
				.map(response -> new ResponseEntity<Void>(HttpStatus.OK))				
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}	
}
