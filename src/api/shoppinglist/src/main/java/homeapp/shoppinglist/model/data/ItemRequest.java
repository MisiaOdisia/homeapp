package homeapp.shoppinglist.model.data;

import javax.validation.constraints.NotBlank;

public class ItemRequest {

	private String id;
	@NotBlank
	private String name;
	private boolean isBought;
	
	public ItemRequest() { }
	
	public ItemRequest(@NotBlank String name, boolean isBought) {
		this.name = name;
		this.isBought = isBought;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean isBought() {
		return isBought;
	}
	public void setBought(boolean isBought) {
		this.isBought = isBought;
	}
}
