package homeapp.shoppinglist.model.entity;

public class ItemEntity {

	private String id;
	private String name;
	private Boolean isBought;
	
	public ItemEntity(String id, String name, Boolean isBought) {
		this.id = id;
		this.name = name;
		this.isBought = isBought;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean isBought() {
		return isBought;
	}
	public void setBought(Boolean isBought) {
		this.isBought = isBought;
	}
}
