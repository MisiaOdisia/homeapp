package homeapp.shoppinglist.model.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="shoppinglist")
public class ShoppingListEntity {
	
	@Id
	private String id;
	
	private String name;
	private List<ItemEntity> items;
	
	public ShoppingListEntity() {
		items = new ArrayList<>();
	}
	
	public ShoppingListEntity(String name, List<ItemEntity> items) {
		this.name = name;
		this.items = items;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}
}
