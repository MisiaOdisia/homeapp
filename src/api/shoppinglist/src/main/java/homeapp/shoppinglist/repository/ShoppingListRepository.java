package homeapp.shoppinglist.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import homeapp.shoppinglist.model.entity.ShoppingListEntity;

@Repository
public interface ShoppingListRepository extends MongoRepository<ShoppingListEntity, String> {

}
