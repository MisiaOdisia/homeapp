package homeapp.shoppinglist.model.data;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

public class ShoppingListRequest {
	
	@NotBlank
	private String name;
	private List<ItemRequest> items;
	
	public ShoppingListRequest() {	}
	
	public ShoppingListRequest(@NotBlank String name, List<ItemRequest> items) {
		this.name = name;
		this.items = items;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ItemRequest> getItems() {
		return items;
	}
	public void setItems(List<ItemRequest> items) {
		this.items = items;
	}
}
