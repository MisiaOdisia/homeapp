package homeapp.shoppinglist.converter;

import org.springframework.stereotype.Component;

import homeapp.shoppinglist.model.data.ItemRequest;
import homeapp.shoppinglist.model.data.response.ItemResponse;
import homeapp.shoppinglist.model.entity.ItemEntity;

@Component
public class ItemEntityConverter {
	
	public ItemResponse toResponse(ItemEntity itemEntity) {
		return new ItemResponse(itemEntity.getId(), itemEntity.getName(), itemEntity.isBought());
	}
	
	public ItemEntity fromResponse(ItemRequest itemRequest) {
		return new ItemEntity(itemRequest.getId(), itemRequest.getName(), itemRequest.isBought());
	}
}
