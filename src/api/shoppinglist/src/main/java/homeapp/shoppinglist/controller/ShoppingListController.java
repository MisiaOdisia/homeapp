package homeapp.shoppinglist.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import homeapp.shoppinglist.model.data.ShoppingListRequest;
import homeapp.shoppinglist.model.data.response.ShoppingListResponse;
import homeapp.shoppinglist.service.ShoppingListService;

//@CrossOrigin(origins = "http://localhost:4201")
@RestController
@RequestMapping("api/v1/shoppinglist")
public class ShoppingListController {
	
	@Autowired
	private ShoppingListService shoppingListService;
	
	@GetMapping
	@CrossOrigin(origins = "http://localhost:4201")
	public List<ShoppingListResponse> getAllShoppingLists() {
		return shoppingListService.getShoppingLists();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ShoppingListResponse> getShoppingListById(@PathVariable(value = "id") String id) {
		return shoppingListService.findShoppingListById(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	
	@PostMapping()
	public ShoppingListResponse createShoppingList(@Valid @RequestBody ShoppingListRequest shoppingList) {
		return shoppingListService.saveShoppingList(shoppingList);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<ShoppingListResponse> updateShoppingList(@PathVariable(value = "id") String id, @Valid @RequestBody ShoppingListRequest shoppingList) {
		return shoppingListService.updateShoppingList(id, shoppingList)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteShoppingList(@PathVariable(value = "id") String id) {
		return shoppingListService.deleteShoppingList(id)
				.map(response -> new ResponseEntity<Void>(HttpStatus.OK))
				.orElse(ResponseEntity.notFound().build());	
	}
}
