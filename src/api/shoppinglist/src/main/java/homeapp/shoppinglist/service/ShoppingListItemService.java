package homeapp.shoppinglist.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import homeapp.shoppinglist.converter.ItemEntityConverter;
import homeapp.shoppinglist.model.data.ItemRequest;
import homeapp.shoppinglist.model.data.response.ItemResponse;
import homeapp.shoppinglist.model.entity.ItemEntity;
import homeapp.shoppinglist.repository.ShoppingListRepository;
import homeapp.shoppinglist.utils.OperationStatus;

@Service
public class ShoppingListItemService {
	
	@Autowired
	private ShoppingListRepository shoppingListRepository;
	@Autowired
	private ItemEntityConverter itemEntityConverter;
	
	public Optional<List<ItemResponse>> getItems(String id) {
		return shoppingListRepository.findById(id)
				.map(shoppingList -> shoppingList.getItems()
						.stream()
						.map(itemEntityConverter::toResponse)
						.collect(Collectors.toList())
					);
	}
	
	public Optional<ItemResponse> getItemById(String shoppingListId, String itemId) {
		return shoppingListRepository.findById(shoppingListId)
				.map(shoppingList -> itemEntityConverter.toResponse(findItemById(shoppingList.getItems(), itemId)));
	}
	
	public Optional<ItemResponse> addItemToList(String shoppingListId, ItemRequest item) {
		 return shoppingListRepository.findById(shoppingListId)
			.map(shoppingList -> {
				String itemId = UUID.randomUUID().toString();
				item.setId(itemId);
				shoppingList.getItems().add(itemEntityConverter.fromResponse(item));
				
				shoppingListRepository.save(shoppingList);				
				
				return itemEntityConverter.toResponse(findItemById(shoppingList.getItems(), itemId));
			});
	}
	
	public Optional<OperationStatus> removeItemFromList(String shoppingListId, String itemId) {
		return shoppingListRepository.findById(shoppingListId)
		.map(shoppingList -> {
			List<ItemEntity> items = shoppingList.getItems();
			
			items.remove(findItemById(items, itemId));					
			shoppingList.setItems(items);
			shoppingListRepository.save(shoppingList);
			
			return OperationStatus.SUCCESS;		
		});
	}
	
	private ItemEntity findItemById(List<ItemEntity> items, String itemId) {
		return items.stream()
				.filter(item -> item.getId().equals(itemId))
				.findFirst()
				.orElseThrow();
	}
}
