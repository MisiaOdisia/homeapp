package homeapp.shoppinglist.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import homeapp.shoppinglist.converter.ShoppingListEntityConverter;
import homeapp.shoppinglist.model.data.ShoppingListRequest;
import homeapp.shoppinglist.model.data.response.ShoppingListResponse;
import homeapp.shoppinglist.model.entity.ShoppingListEntity;
import homeapp.shoppinglist.repository.ShoppingListRepository;
import homeapp.shoppinglist.utils.OperationStatus;

@Service
public class ShoppingListService {

	@Autowired
	private ShoppingListRepository shoppingListRepository;
	@Autowired
	private ShoppingListEntityConverter shoppingListEntityConverter;
	
	public List<ShoppingListResponse> getShoppingLists() {
		return shoppingListRepository.findAll()
				.stream()
				.map(shoppingListEntityConverter::toResponse)
				.collect(Collectors.toList());
	}
	
	public Optional<ShoppingListResponse> findShoppingListById(String id) {
		return shoppingListRepository.findById(id)
				.map(shoppingListEntityConverter::toResponse);
	}
	
	public ShoppingListResponse saveShoppingList(ShoppingListRequest shoppingListData) {
		ShoppingListEntity shoppingListEntity = 
				shoppingListRepository.save(shoppingListEntityConverter.fromResponse(shoppingListData));
		
		return shoppingListEntityConverter.toResponse(shoppingListEntity);
	}
	
	public Optional<ShoppingListResponse> updateShoppingList(String id, ShoppingListRequest shoppingListData) {
		return shoppingListRepository.findById(id)
			.map(shoppingList -> {				
				shoppingList.setName(shoppingListData.getName());
				shoppingList.setItems(shoppingListEntityConverter.getItemsFromRequest(shoppingListData));
				
				return shoppingListEntityConverter.toResponse(shoppingListRepository.save(shoppingList));
			});
	}
	
	public Optional<OperationStatus> deleteShoppingList(String id) {
		return shoppingListRepository.findById(id)
		.map(shoppingListEntity -> {
			shoppingListRepository.delete(shoppingListEntity);
			return OperationStatus.SUCCESS;
		});
	}
}
