package homeapp.shoppinglist.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import homeapp.shoppinglist.model.data.ShoppingListRequest;
import homeapp.shoppinglist.model.data.response.ItemResponse;
import homeapp.shoppinglist.model.data.response.ShoppingListResponse;
import homeapp.shoppinglist.model.entity.ItemEntity;
import homeapp.shoppinglist.model.entity.ShoppingListEntity;

@Component
public class ShoppingListEntityConverter {
	
	@Autowired
	private ItemEntityConverter itemEntityConverter;
	
	public ShoppingListResponse toResponse(ShoppingListEntity shoppingListEntity) {		
		List<ItemResponse> itemDatas = shoppingListEntity.getItems()
									.stream()
									.map(itemEntityConverter::toResponse)
									.collect(Collectors.toList());
		
		return new ShoppingListResponse(shoppingListEntity.getId(), shoppingListEntity.getName(), itemDatas);
	}
	
	public ShoppingListEntity fromResponse(ShoppingListRequest shoppingListData) {		
		return new ShoppingListEntity(shoppingListData.getName(), getItemsFromRequest(shoppingListData));
	}
	
	public List<ItemEntity> getItemsFromRequest(ShoppingListRequest shoppingListData) {		
		if(shoppingListData.getItems() != null) 		
			return shoppingListData.getItems()
					.stream()
					.map(itemEntityConverter::fromResponse)
					.collect(Collectors.toList());
		 else 
			return new ArrayList<>();
	}
}
