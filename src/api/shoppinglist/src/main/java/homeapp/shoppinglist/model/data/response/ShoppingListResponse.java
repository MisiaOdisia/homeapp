package homeapp.shoppinglist.model.data.response;

import java.util.List;

import homeapp.shoppinglist.model.entity.ItemEntity;

public class ShoppingListResponse {
	
	private String id;	
	private String name;
	private List<ItemResponse> items;
	
	public ShoppingListResponse() {	}
	
	public ShoppingListResponse(String id, String name, List<ItemResponse> items) {
		this.id = id;
		this.name = name;
		this.items = items;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ItemResponse> getItems() {
		return items;
	}

	public void setItems(List<ItemResponse> items) {
		this.items = items;
	}
}
