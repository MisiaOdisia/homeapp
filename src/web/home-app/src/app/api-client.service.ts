import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShoppingList } from './shopping-list';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {

  constructor(private httpClient: HttpClient) { }

  public getShoppingLists() {
    return this.httpClient.get<ShoppingList[]>("http://localhost:8080/api/v1/shoppinglist");
  }

  public deleteShoppingList(id:string) {
    return this.httpClient.delete("http://localhost:8080/api/v1/shoppinglist/"+ id);
  }
}
