import { Component, OnInit } from '@angular/core';
import { ApiClientService } from '../api-client.service';
import { Item } from '../dto/item';
import { ShoppingList } from '../shopping-list';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  shoppingLists : ShoppingList[] = null; 

  constructor(private apiClientService: ApiClientService) { }

  ngOnInit() {
    this.apiClientService.getShoppingLists()
      .subscribe(res => {
          console.log(res);
          this.shoppingLists = res.map(dto => new ShoppingList(dto.id, dto.name, dto.items));
          console.log(this.shoppingLists);
      });
  }

}
