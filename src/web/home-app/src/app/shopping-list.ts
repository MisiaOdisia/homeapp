import { Item } from './dto/item';

export class ShoppingList {
    constructor(public id: string, public name: String, public items: Item[]) { }

    public getItemsCount() {
        if(this.items === null)
            return 0;

        return this.items.length;
    }

    public getBoughtItemsCount() {
        if(this.items === null)
            return 0;

        return this.items.filter(item => item.isBought).length;
    }
}
